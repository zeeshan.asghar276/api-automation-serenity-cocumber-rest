# The project is for the sole purpose of given Assignment

This tutorial show you how to get started with REST-API testing using Serenity and Cucumber 6. 

## Get the code

GitLab Link to Repo:
    https://gitlab.com/zeeshan.asghar276/api-automation-serenity-cocumber-rest.git

## The Serenity project
The project is developed as per the required technologies given in the assignment.

- CI Platform: gitlab
- BDD:Cucumber/ Ghirkin
- Framework: Java Serenity

## Tasks performed

Following tasks are perfomred in the given project as per the assignment requirement.

- Automate Endpoints inside the CI pipeline
- Cover positive and Negative scenerios
- Write Instruciton in Readme for installation and execution
- Set Html reports tool
- Provide GitLab link to your rapo

### The project directory structure
The project has build scripts for both Maven and Gradle, and follows the standard directory structure used in most Serenity projects:
```Gherkin
src
  + main
  + test
    + java                                Test runners and supporting code
    + resources
      + features                          Feature files 
          + features.feature 
            + check_the_application_status
             
      + templates                         Freemarker templates and properties files                

```

## First Positive GET scenario
The project comes with two simple scenarios using GET requests ( one Postitive and one Negative)

The first scenario exercises the `https://instagram47.p.rapidapi.com/user_following` endpoint

In this scenario, User gets the followers by hitting the above user and verifies 200 reponse code:

```Gherkin
   Scenario: Application status end-point
    Given I am an authorized user
    When I hit the url
    Then the API should return Status code 200
```
## Second Negative GET scenario

The first scenario exercises the `https://instagram47.p.rapidapi.com/user_following` endpoint

In this scenario, User when user hitd the URl without authentication, the user gets the 401 response code.

```gherkin
Feature: Verify 401 response 

  Scenario: Application status Negative Test
     Given I am an un authorized user
     When I hit the url
     Then the API should return Status code 401

```

## CI/CD Integration
- A pipline has been created for the project
- Testing job has been created inside the pipeline 
- Pipline will execute the testing job on every code commit/push in the master branch

## Execution Instrucitons in local system:
1- Clone the Project by above provided repository link
2- Install mvn in your system (if not present) to run the project locally
3- Create account on rapid api and go to the link "https://rapidapi.com/Prasadbro/api/instagram47"
4- Copy the X-RapidAPI-Key and replace the key in given scenarios

Scenario: Application status end-point
    Given I am an authorized user "Pase key here"
    When I hit the url
    Then the API should return Status code 200

  Scenario: Application status Negative Test
     Given I am an un authorized user "Pase key here"
     When I hit the url
     Then the API should return Status code 401

5- click on **subscribe to test** for subscription and enter all details otherwise api will throw 403 forbidden response


6- Run "mvn clean verify" commnad to see the project results.


## Serenity Reports

A report will be generted every time you run the tests using commnad `mvn clean verify`. Follow below steps to view the results.

1- Go to "api-automation-serenity-cocumber\target\site\serenity\index.html" path.
2- Open index.html in browser and view the test results report in html format.

