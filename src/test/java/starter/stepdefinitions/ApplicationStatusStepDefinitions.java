package starter.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;
import io.restassured.response.Response;


public class ApplicationStatusStepDefinitions {
    private static Response response;

    @Steps

    RequestSpecification request = RestAssured.given();

    @Given("I am an authorized user {string}")
    public void i_am_an_authorized_user(String token) {
        request.header("x-rapidapi-key", token );
        request.header("x-rapidapi-host", "instagram47.p.rapidapi.com");
        request.header("useQueryString", true);
        request.param("userid","1718924098");
        response = request.get("https://instagram47.p.rapidapi.com/user_following");
    }
    @Given("I am an un authorized user {string}")
    public void i_am_an_un_authorized_user(String token) {
        request.header("x-rapidapi-key", token);
        request.header("x-rapidapi-host", "instagram47.p.rapidapi.com");
        request.header("useQueryString", true);
        request.param("userid","1718924098");
        response = request.get("https://instagram47.p.rapidapi.com/user_following");
    }

    @When("I hit the url")
    public void i_check_the_application_status() {
        response = request.get("https://instagram47.p.rapidapi.com/user_following");
    }
    @Then("the API should return Status code {int}")
    public void the_api_should_return_status_code(int statusCode) {
        // Write code here that turns the phrase above into concrete actions
        System.out.println(response.getStatusCode());
        Assert.assertEquals(statusCode, response.getStatusCode());
    }


}
