Feature: Check on application status

  The `/api/status` end point returns a status message to indicate that the application is running successfully.

  Scenario: Application status end-point
    Given I am an authorized user "1ac1fcc716msh9c40dddd375f22ap1e13b6jsn3230466bf349"
    When I hit the url
    Then the API should return Status code 200

  Scenario: Application status Negative Test
     Given I am an un authorized user "1ac1fcc716msh9c40dddd375f22ap1e13b6jsn3230466bf349"
     When I hit the url
     Then the API should return Status code 401